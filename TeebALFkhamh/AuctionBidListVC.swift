//
//  AuctionBidListVC.swift
//  TeebALFkhamh
//
//  Created by MacBook Pro on 6/11/19.
//  Copyright © 2019 yogesh. All rights reserved.
//

import UIKit

class AuctionBidListVC: UIViewController ,UITableViewDataSource{

    let defaults = UserDefaults.standard;

    var productId:String!
    var auctionId:String!
    
    var namesList = [String]()
    var amountsList = [String]()
    var timeList = [String]()
    
    
    @IBOutlet weak var biddersTableView: UITableView!
    @IBOutlet weak var noBidsLabel: UILabel!
    @IBOutlet weak var bidderNameLabel: UILabel!
    @IBOutlet weak var biddingAmountLabel: UILabel!
    @IBOutlet weak var timeStampLabel: UILabel!
    
    
    override func viewDidLoad() {
        super.viewDidLoad()
        
        self.navigationItem.title = NetworkManager.sharedInstance.language(key: "bidList")

        noBidsLabel.text = NetworkManager.sharedInstance.language(key: "noBidFound")
        bidderNameLabel.text = NetworkManager.sharedInstance.language(key: "bidderName")
        biddingAmountLabel.text = NetworkManager.sharedInstance.language(key: "biddingAmount")
        timeStampLabel.text = NetworkManager.sharedInstance.language(key: "timeStamp")

        biddersTableView.dataSource = self
        
        callingBidListApi()
        
    }
    
    func callingBidListApi(){
        
        DispatchQueue.main.async{
            NetworkManager.sharedInstance.showLoader()
            let sessionId = self.defaults.object(forKey:"wk_token");
            
            var requstParams = [String:String]();
            requstParams["wk_token"] = sessionId as? String
            
            NetworkManager.sharedInstance.callingHttpRequest(params:requstParams, apiname:"auction/bidlist&product_id=\(self.productId!)&auction_id=\(self.auctionId!)", cuurentView: self){val,responseObject in
                
                if val == 1 {
                    
                    let dict = JSON(responseObject as! NSDictionary)
                    let productsDataJson = dict.dictionaryValue["products"]!
                    self.parseBidListData(bidderJsonArray: productsDataJson)
                    
                }else if val == 3{
                    
                    let responseObject = responseObject as! JSON
                    let productsDataJson = responseObject.dictionaryValue["products"]!
                    self.parseBidListData(bidderJsonArray: productsDataJson)
                    
                }
                
            }
            
        }
    }
    
    func parseBidListData(bidderJsonArray : JSON){
        
        if bidderJsonArray.arrayValue.count > 0 {
            
            self.namesList = [String]()
            self.amountsList = [String]()
            self.timeList = [String]()
            
            for json in bidderJsonArray.arrayValue {
                self.namesList.append(json["bidder_name"].stringValue)
                self.amountsList.append(json["bid_amount"].stringValue)
                self.timeList.append(json["bid_time"].stringValue)

            }
            
            
            biddersTableView.reloadData()


            self.biddersTableView.isHidden = false
            self.noBidsLabel.isHidden = true
            
            if !bidderJsonArray.arrayValue[0]["bidder_name"].exists(){
                self.noBidsLabel.isHidden = false
                self.biddersTableView.isHidden = true
            }
            
        }else {
            self.noBidsLabel.isHidden = false
            self.biddersTableView.isHidden = true
        }
        
        NetworkManager.sharedInstance.dismissLoader()
        
        
    }

    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        
        return namesList.count
    }
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        //BidderInListCell
        let cell:BidderInListCell = tableView.dequeueReusableCell(withIdentifier: "BidderInListCell") as! BidderInListCell
        
        cell.setData(name: self.namesList[indexPath.row], amount: self.amountsList[indexPath.row], time: self.timeList[indexPath.row])
        
        return cell
    }
    
    /*
    // MARK: - Navigation
    // In a storyboard-based application, you will often want to do a little preparation before navigation
    override func prepare(for segue: UIStoryboardSegue, sender: Any?) {
        // Get the new view controller using segue.destination.
        // Pass the selected object to the new view controller.
    }
    */

}
