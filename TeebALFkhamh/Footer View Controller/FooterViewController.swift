//
//  FooterViewController.swift
//  Bierbrunnen
//
//  Created by kunal on 12/09/18.
//  Copyright © 2018 kunal. All rights reserved.
//

import UIKit

class FooterViewController: UIViewController,UIWebViewDelegate {

@IBOutlet weak var webview: UIWebView!
var id:String = ""
var titleData:String = ""
    
    
    

    override func viewDidLoad() {
        super.viewDidLoad()
        self.navigationItem.title = titleData
        self.navigationController?.isNavigationBarHidden = false
        webview.delegate = self
        self.callingHttppApi()
    }
    
    override func viewWillAppear(_ animated: Bool) {
        self.navigationController?.isNavigationBarHidden = false
    }

   
    func loginRequest(){
        var loginRequest = [String:String]();
        loginRequest["apiKey"] = API_USER_NAME;
        loginRequest["apiPassword"] = API_KEY.md5;
        NetworkManager.sharedInstance.callingHttpRequest(params:loginRequest, apiname:"common/apiLogin", cuurentView: self){val,responseObject in
            if val == 1{
                let dict = responseObject as! NSDictionary
                sharedPrefrence.set(dict.object(forKey: "wk_token") as! String, forKey: "wk_token")
                sharedPrefrence.set(dict.object(forKey: "language") as! String, forKey: "language")
                sharedPrefrence.set(dict.object(forKey: "currency") as! String, forKey: "currency")
                sharedPrefrence.synchronize();
                self.callingHttppApi()
            }else if val == 2{
                NetworkManager.sharedInstance.dismissLoader()
                self.loginRequest()
            }
        }
    }
    
    func callingHttppApi(){
        DispatchQueue.main.async{
            self.view.isUserInteractionEnabled = false
            NetworkManager.sharedInstance.showLoader()
            let sessionId = sharedPrefrence.object(forKey:"wk_token");
            var requstParams = [String:Any]();
            requstParams["wk_token"] = sessionId;
            requstParams["information_id"] = self.id
          
                NetworkManager.sharedInstance.callingHttpRequest(params:requstParams, apiname:"common/getInformationPage", cuurentView: self){success,responseObject in
                    if success == 1 {
                        self.view.isUserInteractionEnabled = true
                        let dict = JSON(responseObject as! NSDictionary)
                        if dict["fault"].intValue == 1{
                                self.loginRequest()
                        }else{
                            if dict["error"].intValue == 0{
                                self.webview.loadHTMLString(dict["description"].stringValue, baseURL: nil)
                            }
                        }
                        
                    }else if success == 2{
                        NetworkManager.sharedInstance.dismissLoader()
                        self.callingHttppApi();
                    }
                }
        }
    }
    
    
    
    
    func webViewDidFinishLoad(_ webView: UIWebView) {
        NetworkManager.sharedInstance.dismissLoader()
        
    }
    
    func webView(_ webView: UIWebView, didFailLoadWithError error: Error) {
        NetworkManager.sharedInstance.dismissLoader()
        
    }
    
    
    override func viewWillDisappear(_ animated: Bool) {
        NetworkManager.sharedInstance.dismissLoader()
    }

}
