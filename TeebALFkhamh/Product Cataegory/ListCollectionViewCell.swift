//
//  ListCollectionViewCell.swift
//  Magento2MobikulNew
//
//  Created by Kunal Parsad on 09/08/17.
//  Copyright © 2017 Kunal Parsad. All rights reserved.
//

import UIKit

class ListCollectionViewCell: UICollectionViewCell {
    
    
@IBOutlet weak var imageView: UIImageView!
@IBOutlet weak var name: UILabel!
@IBOutlet weak var descriptionData: UILabel!
@IBOutlet weak var price: UILabel!
@IBOutlet weak var quickView: UIButton!
@IBOutlet weak var specialPriceLabel: UILabel!
    
    var releaseDate:Date?

    @IBOutlet weak var timerCounterView: UIView!
    @IBOutlet weak var daysLabel: UILabel!
    @IBOutlet weak var hoursLabel: UILabel!
    @IBOutlet weak var minuteLabel: UILabel!
    @IBOutlet weak var secondLabel: UILabel!
    @IBOutlet weak var daysTitle: UILabel!
    @IBOutlet weak var hourTitle: UILabel!
    @IBOutlet weak var minuteTitle: UILabel!
    @IBOutlet weak var secondTitle: UILabel!
    
    override func awakeFromNib() {
        super.awakeFromNib()
        // Initialization code
        quickView.setTitle(NetworkManager.sharedInstance.language(key: "addtocart"), for: .normal)
        quickView.layer.cornerRadius = 5;
        quickView.layer.masksToBounds = true
        quickView.backgroundColor = UIColor.white
        
        specialPriceLabel.isHidden = true;
        
        
        timerCounterView.layer.borderColor = UIColor.init(red: 255/255, green: 152/255, blue: 0, alpha: 1).cgColor
        timerCounterView.layer.borderWidth = 2
        
        timerCounterView.layer.cornerRadius = 7
        
        daysTitle.text = NetworkManager.sharedInstance.language(key: "days")
        hourTitle.text = NetworkManager.sharedInstance.language(key: "hour")
        minuteTitle.text = NetworkManager.sharedInstance.language(key: "minute")
        secondTitle.text = NetworkManager.sharedInstance.language(key: "second")
        
        
    }
    
    
    func startTimer(releaseDateString : String){
        let releaseDateFormatter = DateFormatter()
        releaseDateFormatter.dateFormat = "yyyy-MM-dd HH:mm:ss"
        releaseDate = releaseDateFormatter.date(from:releaseDateString)!
        Timer.scheduledTimer(timeInterval: 1, target: self, selector: #selector(self.countDownDate), userInfo: nil, repeats: true)
    }
    
    @objc func countDownDate() {
        let date = Date()
        let calendar = Calendar.current
        
        let diffDateComponents = calendar.dateComponents([.day , .hour , .minute , .second], from: date, to: releaseDate!)
        
        daysLabel.text = "\(diffDateComponents.day!)"
        hoursLabel.text = "\(diffDateComponents.hour!)"
        minuteLabel.text = "\(diffDateComponents.minute!)"
        secondLabel.text = "\(diffDateComponents.second!)"
        
        //let countdown = "Days \(diffDateComponents.day),  Hours: \(diffDateComponents.hour), Minutes: \(diffDateComponents.minute), Seconds: \(diffDateComponents.second)"
        //print(countdown)
    }

}
