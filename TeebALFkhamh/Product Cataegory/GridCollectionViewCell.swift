//
//  GridCollectionViewCell.swift
//  TeebALFkhamh
//
//  Created by MacBook Pro on 6/10/19.
//  Copyright © 2019 yogesh. All rights reserved.
//

import UIKit

class GridCollectionViewCell: UICollectionViewCell {

    // SAMUEl
    var releaseDate:Date?

    @IBOutlet weak var productImage: UIImageView!
    @IBOutlet weak var productName: UILabel!
    @IBOutlet weak var productPrice: UILabel!
    @IBOutlet weak var specialPrice: UILabel!
    @IBOutlet weak var wishListButton: UIButton!
    @IBOutlet weak var addToCartButton: UIButton!
    @IBOutlet var saleLabel: UILabel!

    @IBOutlet weak var timerCounterView: UIView!
    @IBOutlet weak var daysLabel: UILabel!
    @IBOutlet weak var hoursLabel: UILabel!
    @IBOutlet weak var minuteLabel: UILabel!
    @IBOutlet weak var secondLabel: UILabel!
    @IBOutlet weak var daysTitle: UILabel!
    @IBOutlet weak var hourTitle: UILabel!
    @IBOutlet weak var minuteTitle: UILabel!
    @IBOutlet weak var secondTitle: UILabel!

    
    override func awakeFromNib() {
        super.awakeFromNib()
        // Initialization code
        specialPrice.isHidden = true
        addToCartButton.setTitleColor(UIColor.white, for: .normal)
        addToCartButton.backgroundColor = UIColor().HexToColor(hexString: BUTTON_COLOR)
        addToCartButton.setTitle(NetworkManager.sharedInstance.language(key: "addtocart"), for: .normal)
        addToCartButton.layer.cornerRadius = 5;
        addToCartButton.layer.masksToBounds = true
        //productName.textColor = UIColor().HexToColor(hexString: LIGHTGREY)
        saleLabel.text = " "+"sale".localized+" "
        saleLabel.layer.cornerRadius = 3;
        saleLabel.layer.masksToBounds = true
        //saleLabel.backgroundColor = UIColor().HexToColor(hexString: BUTTON_COLOR)
        saleLabel.isHidden = true
        
        timerCounterView.layer.borderColor = UIColor.init(red: 255/255, green: 152/255, blue: 0, alpha: 1).cgColor
        timerCounterView.layer.borderWidth = 2
        
        timerCounterView.layer.cornerRadius = 7
        
        daysTitle.text = NetworkManager.sharedInstance.language(key: "days")
        hourTitle.text = NetworkManager.sharedInstance.language(key: "hour")
        minuteTitle.text = NetworkManager.sharedInstance.language(key: "minute")
        secondTitle.text = NetworkManager.sharedInstance.language(key: "second")
    }
    
    func startTimer(releaseDateString : String){
        let releaseDateFormatter = DateFormatter()
        releaseDateFormatter.dateFormat = "yyyy-MM-dd HH:mm:ss"
        releaseDate = releaseDateFormatter.date(from:releaseDateString)!
        Timer.scheduledTimer(timeInterval: 1, target: self, selector: #selector(self.countDownDate), userInfo: nil, repeats: true)
    }
    
    @objc func countDownDate() {
        let date = Date()
        let calendar = Calendar.current
        
        let diffDateComponents = calendar.dateComponents([.day , .hour , .minute , .second], from: date, to: releaseDate!)
        
        daysLabel.text = "\(diffDateComponents.day!)"
        hoursLabel.text = "\(diffDateComponents.hour!)"
        minuteLabel.text = "\(diffDateComponents.minute!)"
        secondLabel.text = "\(diffDateComponents.second!)"
        
        //let countdown = "Days \(diffDateComponents.day),  Hours: \(diffDateComponents.hour), Minutes: \(diffDateComponents.minute), Seconds: \(diffDateComponents.second)"
        //print(countdown)
    }

}
