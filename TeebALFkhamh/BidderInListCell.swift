//
//  BidderInListCell.swift
//  TeebALFkhamh
//
//  Created by MacBook Pro on 6/11/19.
//  Copyright © 2019 yogesh. All rights reserved.
//

import UIKit

class BidderInListCell: UITableViewCell {

    @IBOutlet weak var bidderNameLabel: UILabel!
    @IBOutlet weak var biddingAmountLabel: UILabel!
    @IBOutlet weak var timeStampLabel: UILabel!
    
    override func awakeFromNib() {
        super.awakeFromNib()
        // Initialization code
    }

    override func setSelected(_ selected: Bool, animated: Bool) {
        super.setSelected(selected, animated: animated)

        // Configure the view for the selected state
    }
    
    func setData(name:String , amount : String , time:String) {
        
        self.bidderNameLabel.text = name
        self.biddingAmountLabel.text = amount
        self.timeStampLabel.text = time 
        
    }

}
