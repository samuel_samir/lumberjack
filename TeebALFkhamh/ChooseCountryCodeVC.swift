//
//  ChooseCountryCodeVC.swift
//  TeebALFkhamh
//
//  Created by MacBook Pro on 7/8/19.
//  Copyright © 2019 yogesh. All rights reserved.
//

import UIKit
import CountryPicker


class ChooseCountryCodeVC: UIViewController ,CountryPickerDelegate {
    
    @IBOutlet weak var dialogView: UIView!
    @IBOutlet weak var picker: CountryPicker!
    @IBOutlet weak var doneButton: UIButton!
    
    
    var phoneCode : String = ""
    var flag : UIImage? = nil
    
    override func viewDidLoad() {
        super.viewDidLoad()
        
        doneButton.setTitle(NetworkManager.sharedInstance.language(key: "done"), for: .normal)
        
        // make view rounded with border
        self.dialogView.layer.cornerRadius = 10
        self.dialogView.clipsToBounds = true
        self.dialogView.layer.borderWidth = 2
        self.dialogView.layer.borderColor = UIColor.gray.cgColor
        
        //get current country
        let locale = Locale.current
        let code = (locale as NSLocale).object(forKey: NSLocale.Key.countryCode) as! String?
        
        
        picker.countryPickerDelegate = self
        picker.showPhoneNumbers = true
        if phoneCode != "" {
            
            print("phoneCode")
            print(phoneCode)
            picker.setCountryByPhoneCode("+\(phoneCode)")
            
        }else {
            picker.setCountry(code!)
        }
    }
    
    // a picker item was selected
    func countryPhoneCodePicker(_ picker: CountryPicker, didSelectCountryWithName name: String, countryCode: String, phoneCode: String, flag: UIImage) {
        //pick up anythink
        
        
        self.phoneCode = phoneCode.substring(from: phoneCode.index(after: phoneCode.firstIndex(of: "+")!))
        self.flag = flag
    }
    
    @IBAction func cancelButtonAction(_ sender: Any) {
        self.view.removeFromSuperview()
    }
    
    @IBAction func okButtonAction(_ sender: Any) {
        
        //get parent and set country code and image
        
        if let parent = self.parent {
            if let parent = parent as? CreateAccount{
                
                parent.countryButton.setImage(flag, for: .normal)
                parent.phoneCode = self.phoneCode
                self.view.removeFromSuperview()
                
            }else if let parent = parent as? EditAccountInformation {
                parent.editAccountInformationViewModel.addrerssReceivedModel.code_c = self.phoneCode
                parent.tableView.reloadData()
                self.view.removeFromSuperview()

            }
            
        }
        
    }
    
    override func touchesBegan(_ touches: Set<UITouch>, with event: UIEvent?) {
        if touches.first != nil {
            print("ChooseCountryCodeVC touched")
        }
    }
    
}

