//
//  MainViewTabBarController.swift
//  TeebALFkhamh
//
//  Created by MacBook Pro on 7/8/19.
//  Copyright © 2019 yogesh. All rights reserved.
//

import UIKit

class MainViewTabBarController: UITabBarController {
    let defaults = UserDefaults.standard;

    override func viewDidLoad() {
        super.viewDidLoad()
        // Do any additional setup after loading the view.
    
        //check if user login then call check model api
        let customerId = self.defaults.object(forKey: "customer_id")
        if(customerId != nil){
            checkModelApi()
        }
    }
    
    func checkModelApi(){
//        DispatchQueue.main.async{
//            let sessionId = self.defaults.object(forKey:"wk_token");
//            
//            var requstParams = [String:String]();
//            requstParams["wk_token"] = sessionId as? String
//            
//            NetworkManager.sharedInstance.callingHttpRequest(params:requstParams, apiname:"common/cheekModal", cuurentView: self){val,responseObject in
//                
//                if val == 1 {
//                    
//                    let dict = JSON(responseObject as! NSDictionary)
//                    self.parseModelResponse(data: dict)
//                    
//                }else if val == 3{
//                    
//                    let responseObject = responseObject as! JSON
//                    self.parseModelResponse(data: responseObject)
//                    
//                }
//                
//                
//            }
//            
//        }
    }
    
    func parseModelResponse(data : JSON) {
        
        //samuel
        if data["isverfied"].stringValue == "0" {
            //show dialog
//            self.showEnterCodeDialog(code: data["code"].stringValue)
        }
        
    }
    
    func showEnterCodeDialog(code : String){
        let AC = UIAlertController(title: NetworkManager.sharedInstance.language(key: "verify_phone_number"), message: NetworkManager.sharedInstance.language(key: "enter_code_sent"), preferredStyle: .alert)

        //add text field
        AC.addTextField { (codeTextField) in

        }

        let editBtn = UIAlertAction(title: NetworkManager.sharedInstance.language(key: "edit_phone_number"), style: .default, handler: {(_ action: UIAlertAction) -> Void in

            //go to edit my information page
            self.selectedViewController = self.viewControllers![4]
            if let current = self.selectedViewController as? UINavigationController {
                if let profile = current.viewControllers[0] as? MyProfile {
                    profile.checkModel = false
                    profile.performSegue(withIdentifier: "myProfileToAccountInformation", sender: nil)
                }
            }


        })

        let confirmBtn = UIAlertAction(title: NetworkManager.sharedInstance.language(key: "confirm"), style: .default, handler: {(_ action: UIAlertAction) -> Void in

            let codeTextField = AC.textFields![0] as UITextField

            if code == codeTextField.text {
                //call update code status api
                self.updateCodeStatusApi()

            }else {
                //show error
                NetworkManager.sharedInstance.showErrorSnackBar(msg: NetworkManager.sharedInstance.language(key: "invalid_code"))
                self.showEnterCodeDialog(code: code)
            }

        })

        let resendBtn = UIAlertAction(title: NetworkManager.sharedInstance.language(key: "resend_code"), style: .default, handler: {(_ action: UIAlertAction) -> Void in
            self.resendCodeApi()
            //call resend code api
            self.showEnterCodeDialog(code: code)
        })


        AC.addAction(editBtn)
        AC.addAction(confirmBtn)
        AC.addAction(resendBtn)

        self.present(AC, animated: true, completion: nil)
    }
    
    func updateCodeStatusApi(){
        DispatchQueue.main.async{
            let sessionId = self.defaults.object(forKey:"wk_token");
            
            var requstParams = [String:String]();
            requstParams["wk_token"] = sessionId as? String
            
            NetworkManager.sharedInstance.callingHttpRequest(params:requstParams, apiname:"common/updateCodestatus", cuurentView: self){val,responseObject in
                
                if val == 1 {
                    
                    let dict = JSON(responseObject as! NSDictionary)
                    self.parseCodeUpdateResponse(data: dict)
                    
                }else if val == 3{
                    
                    let responseObject = responseObject as! JSON
                    self.parseCodeUpdateResponse(data: responseObject)
                    
                }
                
                
            }
            
        }
    }

    func parseCodeUpdateResponse(data : JSON) {
        
        if data["success"].stringValue == "1" {
            //show dialog
            NetworkManager.sharedInstance.showSuccessSnackBar(msg: NetworkManager.sharedInstance.language(key: "phone_number_verified"))
        }
        
    }

    func resendCodeApi(){
        DispatchQueue.main.async{
            let sessionId = self.defaults.object(forKey:"wk_token");
            
            var requstParams = [String:String]();
            requstParams["wk_token"] = sessionId as? String
            
            NetworkManager.sharedInstance.callingHttpRequest(params:requstParams, apiname:"common/resendCode", cuurentView: self){val,responseObject in
                
                if val == 1 {
                    
                    let dict = JSON(responseObject as! NSDictionary)
                    self.parseResendCodeResponse(data: dict)
                    
                }else if val == 3{
                    
                    let responseObject = responseObject as! JSON
                    self.parseResendCodeResponse(data: responseObject)
                    
                }
                
                
            }
            
        }
    }
    
    func parseResendCodeResponse(data : JSON) {
        
        if data["success"].stringValue == "1" {
            //show dialog
            NetworkManager.sharedInstance.showSuccessSnackBar(msg: NetworkManager.sharedInstance.language(key: "message_sent_successfully"))
        }
        
    }

    /*
    // MARK: - Navigation

    // In a storyboard-based application, you will often want to do a little preparation before navigation
    override func prepare(for segue: UIStoryboardSegue, sender: Any?) {
        // Get the new view controller using segue.destination.
        // Pass the selected object to the new view controller.
    }
    */

}
