//
/**
TeebALFkhamh
@Category Webkul
@author Webkul <support@webkul.com>
FileName: PhoneTextFieldCell.swift
Copyright (c) 2010-2018 Webkul Software Private Limited (https://webkul.com)
@license   https://store.webkul.com/license.html
*/

import UIKit
import MaterialComponents
import CountryPicker

class PhoneTextFieldCell: UITableViewCell,UITextFieldDelegate {

@IBOutlet var textFieldNormal: MDCTextField!
var usernameTextFieldController: MDCTextInputControllerOutlined!
var delegate:FormFieldTextValueHandler!
    
    var phoneCode : String!
    @IBOutlet weak var countryButton: UIButton!
    
    
    override func awakeFromNib() {
        super.awakeFromNib()
        textFieldNormal.clearButtonMode = .unlessEditing
        textFieldNormal.textColor = UIColor.black  // text color
        textFieldNormal.delegate = self
        usernameTextFieldController = MDCTextInputControllerOutlined(textInput: textFieldNormal)
        usernameTextFieldController.floatingPlaceholderActiveColor = UIColor().HexToColor(hexString: BUTTON_COLOR)
        usernameTextFieldController.activeColor = UIColor().HexToColor(hexString: BUTTON_COLOR)
        textFieldNormal.textAlignment = UITextField().isLanguageLayoutDirectionRightToLeft() ? .right : .left;
        textFieldNormal.clearButton.addTarget(self, action: #selector(clearClick(sender:)), for: .touchUpInside)
        

    }

    @IBAction func chooseCountryCode(_ sender: Any) {
        //get parent to show country picker view
        delegate.getFormFieldValue(val: "", type: "country_code")
    }

    @objc func clearClick(sender: UIButton){
        delegate.getFormFieldValue(val: textFieldNormal.text!, type: textFieldNormal.accessibilityLabel ?? "")
    }

    override func setSelected(_ selected: Bool, animated: Bool) {
        super.setSelected(selected, animated: animated)

        // Configure the view for the selected state
    }
    
    
    
    @IBAction func valueChanged(_ sender: MDCTextField) {
        delegate.getFormFieldValue(val: sender.text!, type: sender.accessibilityLabel ?? "")
    }
    
    func textFieldDidBeginEditing(_ textField: UITextField) {
        while (textField.text?.count)! > 2 && textField.text?.first == "0" {
            textField.text = String((textField.text?.suffix((textField.text?.count)! - 1))!)
        }
        delegate.getFormFieldValue(val: textField.text!, type: textField.accessibilityLabel ?? "")
    }
    
    func textFieldDidEndEditing(_ textField: UITextField) {
        while (textField.text?.count)! > 2 && textField.text?.first == "0" {
            textField.text = String((textField.text?.suffix((textField.text?.count)! - 1))!)
        }
        delegate.getFormFieldValue(val: textField.text!, type: textField.accessibilityLabel ?? "")
    }
    
    
}
