//
//  CategoryCellCollectionViewCell.swift
//  TeebALFkhamh
//
//  Created by MacBook Pro on 6/13/19.
//  Copyright © 2019 yogesh. All rights reserved.
//

import UIKit

class CategoryCellCollectionViewCell: UICollectionViewCell {

    @IBOutlet weak var mainView: UIView!
    @IBOutlet weak var image1: UIImageView!
    @IBOutlet weak var nameLabel: UILabel!
    
    @IBOutlet var childView: UIView!
    
    override func awakeFromNib() {
        super.awakeFromNib()
        
        // Initialization code
        mainView.layer.cornerRadius = 10
        mainView.layer.shadowOffset = CGSize(width: 0, height: 0)
        mainView.layer.shadowRadius = 3
        mainView.layer.shadowOpacity = 0.5
        mainView.layer.masksToBounds = true
        

    }

}
