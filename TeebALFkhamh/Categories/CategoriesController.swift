//
//  CategoriesController.swift
//  OpenCartMpV3
//
//  Created by kunal on 12/12/17.
//  Copyright © 2017 kunal. All rights reserved.
//

import UIKit

class CategoriesController: UIViewController,UITableViewDelegate, UITableViewDataSource ,UICollectionViewDelegate , UICollectionViewDataSource,UICollectionViewDelegateFlowLayout{
   
    @IBOutlet weak var categoriesTableView: UITableView!
    @IBOutlet weak var categoriesCollectionView: UICollectionView!
    
    var cataegoriesCollectionModel = [Categories]()
    var arrayForBool :NSMutableArray = [];
    var categoryName:String = ""
    var categoryId:String = ""
    var categoryDict :NSDictionary = [:]
    var subCategory:NSArray = []
    var subId:String = ""
    var subName:String = ""
    
    override func viewDidLoad() {
        super.viewDidLoad()
        self.navigationItem.title = NetworkManager.sharedInstance.language(key: "categories")
        let paymentViewNavigationController = self.tabBarController?.viewControllers?[0]
        let nav1 = paymentViewNavigationController as! UINavigationController;
        let paymentMethodViewController = nav1.viewControllers[0] as! ViewController
        cataegoriesCollectionModel = paymentMethodViewController.homeViewModel.cataegoriesCollectionModel
        
        
        
        categoriesCollectionView.register(UINib(nibName: "CategoryCellCollectionViewCell", bundle: nil), forCellWithReuseIdentifier: "CategoryCellCollectionViewCell")
        
        categoriesCollectionView.delegate = self
        categoriesCollectionView.dataSource = self
        
        //categoriesTableView.register(UINib(nibName: "CategoryCellTableViewCell", bundle: nil), forCellReuseIdentifier: "CategoryCellTableViewCell")
        //self.categoriesTableView.separatorStyle = .none
        //categoriesTableView.delegate = self;
        //categoriesTableView.dataSource = self;
        //categoriesTableView.separatorColor = UIColor.clear
    }
    
    func collectionView(_ collectionView: UICollectionView, didSelectItemAt indexPath: IndexPath) {
        let isChild = cataegoriesCollectionModel[indexPath.row].isChild
        if isChild{
            subId = cataegoriesCollectionModel[indexPath.row].id
            subName = cataegoriesCollectionModel[indexPath.row].name
            self.performSegue(withIdentifier: "subcategory", sender: self)
        }
        else{
            categoryId = cataegoriesCollectionModel[indexPath.row].id
            categoryName = cataegoriesCollectionModel[indexPath.row].name
            self.performSegue(withIdentifier: "productCategorySegue", sender: self)
        }
    }
    
    func collectionView(_ collectionView: UICollectionView, numberOfItemsInSection section: Int) -> Int {
        return cataegoriesCollectionModel.count
    }
    
    func collectionView(_ collectionView: UICollectionView, cellForItemAt indexPath: IndexPath) -> UICollectionViewCell {
        
        
        let cell:CategoryCellCollectionViewCell = collectionView.dequeueReusableCell(withReuseIdentifier: "CategoryCellCollectionViewCell", for: indexPath) as! CategoryCellCollectionViewCell
        cell.image1.loadImageFrom(url:cataegoriesCollectionModel[indexPath.row].thumbnail , dominantColor: cataegoriesCollectionModel[indexPath.row].dominant_color)
        cell.nameLabel.text = cataegoriesCollectionModel[indexPath.row].name
        return cell
    }
    
    func collectionView(_ collectionView: UICollectionView, layout collectionViewLayout: UICollectionViewLayout, sizeForItemAt indexPath: IndexPath) -> CGSize {
        return CGSize(width: (SCREEN_WIDTH / 2) , height: (SCREEN_WIDTH / 2))
    }
    
    func numberOfSections(in tableView: UITableView) -> Int {
        return 1
    }
    
    func collectionView(_ collectionView: UICollectionView, willDisplay cell: UICollectionViewCell, forItemAt indexPath: IndexPath) {
        cell.backgroundColor = UIColor.white
    }
    
    
    
    
    
    
    
    
    
    
    
    
    func tableView(_ tableView: UITableView, heightForHeaderInSection section: Int) -> CGFloat{
        return 0
    }
    
    func tableView(_ tableView: UITableView, heightForRowAt indexPath: IndexPath) -> CGFloat {
        return SCREEN_WIDTH / 2;
    }
    
    func tableView(_ tableView: UITableView, willDisplay cell: UITableViewCell, forRowAt indexPath: IndexPath) {
        cell.backgroundColor = UIColor.white
    }
    
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        return cataegoriesCollectionModel.count
    }
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        categoriesTableView.register(UINib(nibName: "CategoryCellTableViewCell", bundle: nil), forCellReuseIdentifier: "CategoryCellTableViewCell")
        let cell:CategoryCellTableViewCell = tableView.dequeueReusableCell(withIdentifier: "CategoryCellTableViewCell") as! CategoryCellTableViewCell
        cell.image1.loadImageFrom(url:cataegoriesCollectionModel[indexPath.row].thumbnail , dominantColor: cataegoriesCollectionModel[indexPath.row].dominant_color)
        cell.nameLabel.text = cataegoriesCollectionModel[indexPath.row].name
        return cell
    }
    
    func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath) {
        let isChild = cataegoriesCollectionModel[indexPath.row].isChild
        if isChild{
            subId = cataegoriesCollectionModel[indexPath.row].id
            subName = cataegoriesCollectionModel[indexPath.row].name
            self.performSegue(withIdentifier: "subcategory", sender: self)
        }
        else{
            categoryId = cataegoriesCollectionModel[indexPath.row].id
            categoryName = cataegoriesCollectionModel[indexPath.row].name
            self.performSegue(withIdentifier: "productCategorySegue", sender: self)
        }
    }
    
    override func prepare(for segue: UIStoryboardSegue, sender: Any?) {
        if (segue.identifier == "productCategorySegue") {
            let viewController:Productcategory = segue.destination as UIViewController as! Productcategory
            viewController.categoryType = ""
            viewController.categoryName = self.categoryName
            viewController.categoryId = self.categoryId
        }else if (segue.identifier == "subcategory") {
            let viewController:subCategory = segue.destination as UIViewController as! subCategory
            viewController.subName = subName
            viewController.subId = subId
            
        }
    }
    
}
