//
//  HomeModel.swift
//  OpenCartMpV3
//
//  Created by kunal on 11/12/17.
//  Copyright © 2017 kunal. All rights reserved.
//

import Foundation

struct HomeModal {
    var bannerCollectionModel = [BannerData]()
    var cataegoriesCollectionModel = [Categories]()
    var latestProductCollectionModel = [Products]()
    var layeredProductCollectionModel = [LayerdProducts]()
    var brandProduct = [BrandProducts]()
    var currencyData  = [Currency]()
    var languageData = [Languages]()
    var footerData = [FooterData]()
    
    var cartCount:Int = 0
    
    var guestCheckOut:Bool!
    
    init?(data : JSON) {
        if let arrayData = data["banners"].array{
        bannerCollectionModel =  arrayData.map({(value) -> BannerData in
            return  BannerData(data:value)
        })
        }
        
        if let arrayData1 = data["categories"].array{
        cataegoriesCollectionModel =  arrayData1.map({(value) -> Categories in
            return  Categories(data:value)
        })
        }
        
        if let arrayData2 = data["latestProducts"]["products"].array{
        latestProductCollectionModel =  arrayData2.map({(value) -> Products in
            return  Products(data:value)
        })
        }
        
        if let arrayData3 = data["modules"].array{
        layeredProductCollectionModel =  arrayData3.map({(value) -> LayerdProducts in
            return  LayerdProducts(data:value)
        })
        }
        
        if let arrayData4 = data["carousel"].array{
        brandProduct =  arrayData4.map({(value) -> BrandProducts in
            return  BrandProducts(data:value)
        })
        }
        
        
        if let arrayData5 = data["currencies"]["currencies"].array{
        currencyData =  arrayData5.map({(value) -> Currency in
            return  Currency(data:value)
        })
        }
        
        if let arrayData6 = data["languages"]["languages"].array{
        languageData =  arrayData6.map({(value) -> Languages in
            return  Languages(data:value)
        })
        }
        
        if let arrayData6 = data["footerMenu"].arrayObject{
            footerData =  arrayData6.map({(value) -> FooterData in
                return  FooterData(data:JSON(value))
            })
        }
        
        guestCheckOut =  data["guest_status"].boolValue
        sharedPrefrence.set(data["languages"]["code"].stringValue, forKey: "language")
        sharedPrefrence.set(data["currencies"]["code"].stringValue, forKey: "currency")
        sharedPrefrence.synchronize()
        cartCount = data["cart"].intValue
        
        
    }
    
}


struct FooterData{
    var information_id:String = ""
    var title:String = ""
    
    init(data:JSON) {
        self.information_id = data["information_id"].stringValue
        self.title = data["title"].stringValue.html2String
    }
    
    
}




struct LayerdProducts{
    var link:String = ""
    var title:String = ""
    
    
    var productCollectionModel = [Products]()
    
    init(data:JSON) {
        if let arrayData = data["products"].array{
            productCollectionModel =  arrayData.map({(value) -> Products in
                return  Products(data:value)
            })
        }
        self.link = data["id"].stringValue
        self.title = data["name"].stringValue
    }
    
}



struct BannerData{
    var bannerType:String!
    var imageUrl:String!
    var bannerLink:String!
    var bannerName:String!
    var dominant_color:String = ""
    
    
    init(data:JSON){
        bannerType = data["type"].stringValue
        imageUrl  = data["image"].stringValue
        bannerLink = data["link"].stringValue
        bannerName = data["title"].stringValue
//        self.dominant_color = data["dominant_color"].stringValue
        self.dominant_color = "#626262"

    }
}

struct Categories{
    var id:String!
    var name:String!
    var image:String!
    var thumbnail:String!
    var dominant_color:String = ""
    var dominant_color_icon:String = ""
    var isChild:Bool = false
    
    
    init(data:JSON){
        name  = data["name"].stringValue.html2String
        id = data["path"].stringValue
        image = data["icon"].stringValue
        self.thumbnail = data["image"].stringValue
//        self.dominant_color = data["dominant_color"].stringValue
        self.dominant_color = "#626262"

//        self.dominant_color_icon = data["dominant_color_icon"].stringValue
        self.dominant_color_icon = "#626262"

        isChild = data["child_status"].boolValue
    }
    
}

struct Products{
    var hasOption:Int!
    var name:String!
    var price:String!
    var productID:String!
    var rating:String!
    var ratingValue:Double!
    var specialPrice:Float!
    var image:String!
    var formatted_special:String = ""
    var isInWishList:Int = 0
    var dominant_color:String = ""
    
    init(data:JSON) {
        self.hasOption = data["hasOption"].intValue
        self.name = data["name"].stringValue
        self.price = data["price"].stringValue
        self.productID = data["product_id"].stringValue
        self.rating = data["rating"].stringValue
        self.ratingValue = data["rating"].doubleValue
        self.specialPrice = data["special"].floatValue
        self.image = data["thumb"].stringValue
        self.formatted_special = data["formatted_special"].stringValue
        self.isInWishList = data["wishlist_status"].intValue
//        self.dominant_color = data["dominant_color"].stringValue
        self.dominant_color = "#626262"

    }
}


struct BrandProducts{
    var image:String!
    var  link:String!
    var title:String!
    var dominant_color:String = ""
    
    init(data:JSON) {
        self.image = data["image"].stringValue
        self.link = data["link"].stringValue
        self.title = data["title"].stringValue
//        self.dominant_color = data["dominant_color"].stringValue
        self.dominant_color = "#626262"

    }
}


struct Currency {
    var code:String!
    var title:String!
    init(data:JSON) {
        self.code = data["code"].stringValue
        self.title = data["title"].stringValue
    }
}


struct Languages {
    var code:String!
    var title:String!
    
    init(data:JSON){
        self.code = data["code"].stringValue
        self.title = data["name"].stringValue
    }
}


enum HomeViewModelItemType {
    
    case Category
    case Banner
    case LatestProduct
    case RecentViewData
    case Brand
    case Auction
}

protocol HomeViewModelItem {
    var type: HomeViewModelItemType { get }
    var sectionTitle: String { get }
    var rowCount: Int { get }
}

class HomeViewModelBannerItem: HomeViewModelItem {
    var type: HomeViewModelItemType {
        return .Banner
    }
    
    var sectionTitle: String {
        return ""
    }
    
    var rowCount: Int {
        return bannerCollectionModel.count
    }
    
    var bannerCollectionModel = [BannerData]()
    
    init(categories: [BannerData]) {
        self.bannerCollectionModel = categories
    }
    
}


class HomeViewModelLatestItem: HomeViewModelItem {
    var type: HomeViewModelItemType {
        return .LatestProduct
    }
    
    var sectionTitle: String {
        return ""
    }
    
    var rowCount: Int {
        return productCollectionModel.count
    }
    
    var link:String = ""
    var title:String = ""
    var productCollectionModel = [Products]()
    
    init(categories: LayerdProducts) {
        self.productCollectionModel = categories.productCollectionModel
        self.link = categories.link
        self.title = categories.title
    }
    
}

class HomeViewModelAuctionItem: HomeViewModelItem {
    var type: HomeViewModelItemType {
        return .Auction
    }
    
    var sectionTitle: String {
        return ""
    }
    
    var rowCount: Int {
        return auctionsData.count
    }
    
    var link:String = ""
    var title:String = ""
    var auctionsData = [AuctionModelData]()
    
    init(data: [AuctionModelData]) {
        self.auctionsData = data
        self.title = "auctionProducts".localized
    }
    
}

//
//class HomeViewModelFeatureItem: HomeViewModelItem {
//    var type: HomeViewModelItemType {
//        return .FeatureProduct
//    }
//
//    var sectionTitle: String {
//        return ""
//    }
//
//    var rowCount: Int {
//        return featuredProductCollectionModel.count
//    }
//
//    var featuredProductCollectionModel = [Products]()
//
//    init(categories: [Products]) {
//        self.featuredProductCollectionModel = categories
//    }
//
//}

class HomeViewModelRecentViewItem: HomeViewModelItem    {
    var type: HomeViewModelItemType {
        return .RecentViewData
    }
    
    var sectionTitle: String {
        return ""
    }
    
    var rowCount: Int {
        return recentViewProductData.count
    }
    
    var recentViewProductData = [Productcollection]()
    
    init(categories: [Productcollection]) {
        self.recentViewProductData = categories
    }
    
}

class HomeViewModelBrandItem: HomeViewModelItem {
    var type: HomeViewModelItemType {
        return .Brand
    }
    
    var sectionTitle: String {
        return ""
    }
    
    var rowCount: Int {
        return brandProduct.count
    }
    
    var brandProduct = [BrandProducts]()
    
    init(categories: [BrandProducts]) {
        self.brandProduct = categories
    }
    
}


class HomeViewModelCategoryItem: HomeViewModelItem {
    var type: HomeViewModelItemType {
        return .Category
    }
    
    var sectionTitle: String {
        return ""
    }
    
    var rowCount: Int {
        return cataegoriesCollectionModel.count
    }
    
    var cataegoriesCollectionModel = [Categories]()
    
    init(categories: [Categories]) {
        self.cataegoriesCollectionModel = categories
    }
    
}

struct AuctionModelData{
    var name:String!
    var image:String!
    var currentBid : String!
    var start_time : String!
    var stop_time : String!
    var productID:String!
    var auction_current_price:String!
    var auction_id:String!
    var auction_price:String!
    var reserve_price:String!
    
    
    init(data:JSON) {
        self.name = data["product_name"].stringValue
        self.image = data["product_image"].stringValue
        self.currentBid = data["reserve_price"].stringValue
        self.start_time = data["start_time"].stringValue
        self.stop_time = data["stop_time"].stringValue
        self.productID = data["product_id"].stringValue
        self.auction_current_price = data["auction_current_price"].stringValue
        self.auction_id = data["auction_id"].stringValue
        self.auction_price = data["auction_price"].stringValue
        self.reserve_price = data["reserve_price"].stringValue

    }
    
}

class HomeViewModel : NSObject {
    var items = [HomeViewModelItem]()
    var cataegoriesCollectionModel = [Categories]()
    //var featuredProductCollectionModel = [Products]()
    var latestProductCollectionModel = [Products]()
    var brandProduct = [BrandProducts]()
    var currencyData  = [Currency]()
    var languageData = [Languages]()
    var footerData = [FooterData]()
    var homeViewController:ViewController!
    var guestCheckOut:Bool!
    var cartCount:Int = 0
    var auctionsProductsData = [AuctionModelData]()
    
    func getData(data : JSON  , recentViewData : [Productcollection] , completion:(_ data: Bool) -> Void) {
        guard let data = HomeModal(data: data) else {
            return
        }
        
        items.removeAll()
        
        if !data.bannerCollectionModel.isEmpty {
            let bannerDataCollectionItem = HomeViewModelBannerItem(categories: data.bannerCollectionModel)
            items.append(bannerDataCollectionItem)
        }
        
        if !data.cataegoriesCollectionModel.isEmpty {
            let categoryCollectionItem = HomeViewModelCategoryItem(categories: data.cataegoriesCollectionModel)
            cataegoriesCollectionModel = data.cataegoriesCollectionModel;
            items.append(categoryCollectionItem)
        }

        
        for i in 0..<data.layeredProductCollectionModel.count{
            if data.layeredProductCollectionModel[i].productCollectionModel.count > 0{
              let bannerCollectionItem = HomeViewModelLatestItem(categories: data.layeredProductCollectionModel[i])
              items.append(bannerCollectionItem)
            }
        }
       
        if auctionsProductsData.count > 0 {
            let auctionCollectionItem = HomeViewModelAuctionItem(data: auctionsProductsData)
            items.append(auctionCollectionItem)
        }
        
        if !recentViewData.isEmpty {
            let recentViewCollectionItem = HomeViewModelRecentViewItem(categories: recentViewData)
            items.append(recentViewCollectionItem)
        }
        
        if !data.brandProduct.isEmpty {
            let brandCollectionItem = HomeViewModelBrandItem(categories: data.brandProduct)
            items.append(brandCollectionItem)
            brandProduct = data.brandProduct
        }
        
        if !data.currencyData.isEmpty{
            currencyData = data.currencyData
        }
        if !data.languageData.isEmpty{
            languageData = data.languageData
        }
        if !data.footerData.isEmpty{
            self.footerData = data.footerData
        }
        
        guestCheckOut = data.guestCheckOut
        cartCount = data.cartCount
        
        completion(true)
    }
    
    func getAuctionData(data : JSON) {
        auctionsProductsData = [AuctionModelData]()
        
        guard data.arrayValue[0]["stop_time"].exists() else{return}
        
        for i in 0..<data.arrayValue.count {

            
            let auction = AuctionModelData(data: data.arrayValue[i])
            auctionsProductsData.append(auction)
            
        }
        
        if auctionsProductsData.count > 0 {
            let auctionCollectionItem = HomeViewModelAuctionItem(data: auctionsProductsData)
            items.append(auctionCollectionItem)
        }
        
    }
}



extension HomeViewModel : UITableViewDelegate , UITableViewDataSource {
    public func numberOfSections(in tableView: UITableView) -> Int{
        return items.count ;
        
    }
    
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int{
        
        return 1
    }
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        let item = items[indexPath.section]
        
        switch item.type {
        case .Banner:
            let cell:BannerTableViewCell = tableView.dequeueReusableCell(withIdentifier: BannerTableViewCell.identifier) as! BannerTableViewCell
            cell.delegate = homeViewController
            cell.bannerCollectionModel = ((item as? HomeViewModelBannerItem)?.bannerCollectionModel)!
            return cell;
            
        case .Category:
            let cell:TopCategoryTableViewCell = tableView.dequeueReusableCell(withIdentifier: TopCategoryTableViewCell.identifier) as! TopCategoryTableViewCell
            cell.featureCategoryCollectionModel = ((item as? HomeViewModelCategoryItem)?.cataegoriesCollectionModel)!
            cell.delegate = homeViewController
            cell.categoryCollectionView.reloadData()
            cell.layoutIfNeeded()
            cell.setNeedsLayout()
            return cell;
            
            
        case .LatestProduct:
            let cell:ProductTableViewCell = tableView.dequeueReusableCell(withIdentifier: ProductTableViewCell.identifier) as! ProductTableViewCell
            cell.productCollectionModel = ((item as? HomeViewModelLatestItem)?.productCollectionModel)!
            cell.prodcutCollectionView.reloadData()
            cell.prodcutCollectionView.tag = indexPath.section
            cell.delegate = homeViewController
            cell.homeViewModel = homeViewController.homeViewModel
            cell.frame = tableView.bounds
            cell.layoutIfNeeded()
            cell.prodcutCollectionView.reloadData()
            cell.productCollectionViewHeight.constant = cell.prodcutCollectionView.collectionViewLayout.collectionViewContentSize.height
            cell.newProductLabel.text = ((item as? HomeViewModelLatestItem)?.title)!
            cell.viewAllID = ((item as? HomeViewModelLatestItem)?.link)!
            
            cell.items = self.items
            cell.controller = self.homeViewController
            cell.sectionValue = indexPath.section
            return cell;
            
            
        case .RecentViewData:
            let cell:RecentViewTableViewCell = tableView.dequeueReusableCell(withIdentifier: RecentViewTableViewCell.identifier) as! RecentViewTableViewCell
            cell.recentCollectionModel = ((item as? HomeViewModelRecentViewItem)?.recentViewProductData)!
            cell.recentViewCollectionView.reloadData()
            cell.delegate = homeViewController
            return cell
        
        case .Auction:
            let cell:AuctionViewTableViewCell = tableView.dequeueReusableCell(withIdentifier: AuctionViewTableViewCell.identifier) as! AuctionViewTableViewCell
            
            cell.auctionData = ((item as? HomeViewModelAuctionItem)?.auctionsData)!
            cell.recentViewCollectionView.reloadData()
            cell.delegate = homeViewController
            
            return cell
        
        case .Brand:
            let cell:BrandTableViewCell = tableView.dequeueReusableCell(withIdentifier: BrandTableViewCell.identifier) as! BrandTableViewCell
            cell.brandCollectionModel = ((item as? HomeViewModelBrandItem)?.brandProduct)!
            cell.delegate = homeViewController
            return cell;
            
        }
        
        //return UITableViewCell()
    }
    
    func tableView(_ tableView: UITableView, heightForRowAt indexPath: IndexPath) -> CGFloat {
        let item = items[indexPath.section]
        switch item.type {
        case .Banner:
            return SCREEN_WIDTH / 2
            
        case .Category:
            return 130
            
        case .LatestProduct:
            return UITableView.automaticDimension
        
        case .Auction:
            return SCREEN_WIDTH / 2 + 200
            
        case .RecentViewData:
            return SCREEN_WIDTH / 2 + 140
            
        case .Brand:
            return SCREEN_WIDTH / 3 + 16
            
        }
    }
    
    
    
}
