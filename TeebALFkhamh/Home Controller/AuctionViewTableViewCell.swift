//
//  AuctionViewTableViewCell.swift
//  TeebALFkhamh
//
//  Created by MacBook Pro on 6/9/19.
//  Copyright © 2019 yogesh. All rights reserved.
//

import UIKit


class AuctionViewTableViewCell: UITableViewCell {

    @IBOutlet weak var recentViewLabel: UILabel!
    @IBOutlet weak var recentViewCollectionView: UICollectionView!
    
    var auctionData = [AuctionModelData]()
    var delegate:RecentProductViewControllerHandlerDelegate!
    
    
    override func awakeFromNib() {
        super.awakeFromNib()
        // Initialization code
        
        recentViewCollectionView.register(UINib(nibName: "AuctionProductCell", bundle: nil), forCellWithReuseIdentifier: "AuctionProductCell")
        //recentViewCollectionView.register(UINib(nibName: "ProductImageCell", bundle: nil), forCellWithReuseIdentifier: "productimagecell")

        recentViewCollectionView.delegate = self
        recentViewCollectionView.dataSource = self
        
        recentViewLabel.text = NetworkManager.sharedInstance.language(key: "auctionProducts")
        
    }

    override func setSelected(_ selected: Bool, animated: Bool) {
        super.setSelected(selected, animated: animated)

        // Configure the view for the selected state
    }
    
    static var nib:UINib {
        return UINib(nibName: identifier, bundle: nil)
    }
    
    static var identifier: String {
        return String(describing: self)
    }
    
}

extension AuctionViewTableViewCell: UICollectionViewDelegate, UICollectionViewDataSource , UICollectionViewDelegateFlowLayout{
    
    
    func collectionView(_ collectionView: UICollectionView, numberOfItemsInSection section: Int) -> Int {
        return auctionData.count
    }
    
    func collectionView(_ collectionView: UICollectionView, cellForItemAt indexPath: IndexPath) -> UICollectionViewCell {
        let cell = collectionView.dequeueReusableCell(withReuseIdentifier: "AuctionProductCell", for: indexPath) as! AuctionProductCell
        //let cell = collectionView.dequeueReusableCell(withReuseIdentifier: "productimagecell", for: indexPath) as! ProductImageCell

        cell.myBorder()
        cell.productImage.image = UIImage(named: "ic_placeholder.png")
        cell.productImage.loadImageFrom(url:auctionData[indexPath.row].image , dominantColor: "ffffff")
        cell.productName.text = auctionData[indexPath.row].name
        cell.productPrice.text = auctionData[indexPath.row].auction_price
        cell.layoutIfNeeded()
        cell.startTimer(releaseDateString: auctionData[indexPath.row].stop_time)
        
        
        return cell
    }
    
    func collectionView(_ collectionView: UICollectionView, layout collectionViewLayout: UICollectionViewLayout, sizeForItemAt indexPath: IndexPath) -> CGSize {
        return CGSize(width: collectionView.frame.size.width/2 , height: collectionView.frame.size.width/2 + 130)
    }
    
    func collectionView(_ collectionView: UICollectionView, didSelectItemAt indexPath: IndexPath) {
        if indexPath.row < auctionData.count{
            delegate.recentProductClick(name: auctionData[indexPath.row].name, image: auctionData[indexPath.row].image, id: auctionData[indexPath.row].productID)
        }
    }
    
    
}

