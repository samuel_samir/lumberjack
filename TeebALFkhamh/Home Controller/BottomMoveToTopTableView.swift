//
/**
Mobikul_Magento2V3_App
@Category Webkul
@author Webkul <support@webkul.com>
FileName: BottomMoveToTopTableView.swift
Copyright (c) 2010-2018 Webkul Software Private Limited (https://webkul.com)
@license https://store.webkul.com/license.html ASL Licence
@link https://store.webkul.com/license.html

*/

import UIKit

class BottomMoveToTopTableView: UIView {
    @IBOutlet weak var bottomLabelMessage: UILabel!
    var tableView: UITableView!
    @IBOutlet weak var backToTopButton: UIButton!

    override func layoutSubviews() {
        bottomLabelMessage.text = "bottommessgae".localized
        //bottomLabelMessage.textColor = UIColor().HexToColor(hexString: LIGHTGREY)
        //backToTopButton.setTitleColor(UIColor().HexToColor(hexString: BUTTON_COLOR), for: .normal)
        backToTopButton.setTitle("backtotop".localized, for: .normal)
        backToTopButton.layer.cornerRadius = 10
        backToTopButton.backgroundColor = UIColor().HexToColor(hexString: BUTTON_COLOR)
    }

    @IBAction func backToTopButtonAction(_ sender: Any) {
        scrollToFirstRow()
    }

    func scrollToFirstRow() {
        let indexPath = NSIndexPath(row: 0, section: 0)
        self.tableView.scrollToRow(at: indexPath as IndexPath, at: .top, animated: true)
    }
}
