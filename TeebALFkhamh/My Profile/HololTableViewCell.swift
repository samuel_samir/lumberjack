//
//  HololTableViewCell.swift
//  TeebALFkhamh
//
//  Created by MacBook Pro on 6/13/19.
//  Copyright © 2019 yogesh. All rights reserved.
//

import UIKit

class HololTableViewCell: UITableViewCell {

    @IBOutlet weak var hololLabel: UILabel!
    
    
    override func awakeFromNib() {
        super.awakeFromNib()
        // Initialization code
        hololLabel.text = NetworkManager.sharedInstance.language(key: "poweredByHolol")
    }

    override func setSelected(_ selected: Bool, animated: Bool) {
        super.setSelected(selected, animated: animated)

        // Configure the view for the selected state
    }
    
}
