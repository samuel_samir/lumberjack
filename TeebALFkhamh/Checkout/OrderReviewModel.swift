//
//  OrderReviewModel.swift
//  OpenCartApplication
//
//  Created by Kunal Parsad on 01/09/17.
//  Copyright © 2017 webkul. All rights reserved.
//

import UIKit

class OrderReviewModel: NSObject {
    var productName:String!
    var model:String!
    var price:String!
    var quantity:String!
    var option:Array<JSON>!
    var tax:String!
    var subTotal:String = ""
    var image:String = ""
    
    
    init(data:JSON){
        self.productName = data["name"].stringValue
        self.model = data["model"].stringValue
        self.price = data["price_text"].stringValue
        self.quantity = data["quantity"].stringValue
        self.option = data["option"].arrayValue
        self.tax = data["tax"].stringValue
        self.subTotal = data["total_text"].stringValue
        self.image = data["image"].stringValue
    }

}

class OrderTotalAmount:NSObject{
    var title:String!
    var value:String!
    var text:String!
    
    init(data:JSON) {
        self.title = data["title"].stringValue
        self.value = data["value"].stringValue
        self.text = data["text"].stringValue
        
    }
 
}



class OrderReviewViewModel:NSObject{
   var orderReviewModel = [OrderReviewModel]()
   var orderTotalAmount = [OrderTotalAmount]()
   var billingAddress:String!
   var shippingAddress:String!
   var paymentMethod:String!
   var shipmentMethod:String!
   var paymentURL:String = ""
   var currencyCode:String = ""
   var totalAmount:String = ""

    
    init(data:JSON){
        
        currencyCode = data ["currency_code"].string ?? ""
        for i in 0..<data["continue"]["order_details"]["products"].count{
            let dict = data["continue"]["order_details"]["products"][i];
            orderReviewModel.append(OrderReviewModel(data: dict))
        }
        
        for i in 0..<data["continue"]["totals"].count{
            let dict = data["continue"]["totals"][i];
            orderTotalAmount.append(OrderTotalAmount(data: dict))
            totalAmount = data["continue"]["totals"][i]["value"].string ?? "";
        }
        
        if self.orderTotalAmount.count > 0 {
            totalAmount = self.orderTotalAmount[self.orderTotalAmount.count - 1].value
        }
        
        billingAddress = data["continue"]["order_details"]["firstname"].stringValue+" "+data["continue"]["order_details"]["lastname"].stringValue+"\n"+data["continue"]["order_details"]["payment_company"].stringValue+"\n"+data["continue"]["order_details"]["payment_address_1"].stringValue+","+data["continue"]["order_details"]["payment_postcode"].stringValue+","+data["continue"]["order_details"]["payment_city"].stringValue+"\n"+data["continue"]["order_details"]["payment_zone"].stringValue+","+data["continue"]["order_details"]["payment_country"].stringValue
        
        paymentMethod = data["continue"]["order_details"]["payment_method"].stringValue
        
        shippingAddress = data["continue"]["order_details"]["shipping_firstname"].stringValue+" "+data["continue"]["order_details"]["shipping_lastname"].stringValue+"\n"+data["continue"]["order_details"]["shipping_company"].stringValue+"\n"+data["continue"]["order_details"]["shipping_address_1"].stringValue+","+data["continue"]["order_details"]["shipping_postcode"].stringValue+","+data["continue"]["order_details"]["shipping_city"].stringValue+"\n"+data["continue"]["order_details"]["shipping_zone"].stringValue+","+data["continue"]["order_details"]["shipping_country"].stringValue
        
        shipmentMethod = data["continue"]["order_details"]["shipping_method"].stringValue
        
        self.paymentURL = data["continue"]["apgsenangpay"]["apgsenangpay_url"].stringValue
        
        
    }
    
    
    var getTotalProducts:Array<OrderReviewModel>{
        return orderReviewModel
    }
    
    var getTotalAmount:Array <OrderTotalAmount>{
        return orderTotalAmount
    }
    
    var getBillingAddress:String{
        return billingAddress
    }
    var getShippingAdress:String{
        return shippingAddress
    }
    
    var getPaymentMethodData:String{
        return paymentMethod
    }
    var getShipmentMethodData:String{
        return shipmentMethod
    }
    
    
}

