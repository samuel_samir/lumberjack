//
//  ConfigurationViewModel.swift
//  TeebALFkhamh
//
//  Created by intcore-007 on 4/17/20.
//  Copyright © 2020 yogesh. All rights reserved.
//


import Foundation
import MPGSDK

struct ConfigurationViewModel {
    // MARK: - Inputs
    var merchantId: String?
    var region: GatewayRegion = .mtf
    var merchantServiceURLString: String?
    var applePayMerchantID: String?
    
    // MARK: - Computed Outputs
    var allRegions: [GatewayRegion] { return GatewayRegion.all }
    
    var merchantServiceURL: URL? {
        get {
            guard let urlString = merchantServiceURLString else { return nil }
            return URL(string: urlString)
        }
        set {
            merchantServiceURLString = newValue?.absoluteString
        }
    }
    
    var validMerchantId: Bool {
        guard let id = merchantId else { return false }
        return !id.isEmpty
    }
    
    var validMerchantServiceURL: Bool {
        return merchantServiceURL != nil
    }
    
    var isValid: Bool {
        return validMerchantId && validMerchantServiceURL
    }
    
    // MARK: - Loading and Saving
    func save(toUserDefaults defaults: UserDefaults = .standard) {
        defaults.set(merchantId, forKey: "merchantId")
        defaults.set(region.id, forKey: "region")
        defaults.set(merchantServiceURL, forKey: "merchantServiceURL")
        defaults.set(applePayMerchantID, forKey: "applePayMerchantID")
    }
    
    mutating func load(fromUserDefaults defaults: UserDefaults = .standard) {
        merchantId = defaults.string(forKey: "merchantId")
        if let regionString = defaults.string(forKey: "region"), let new = GatewayRegion.matching(id: regionString) {
            region = new
        } else {
            region = .mtf
        }
        merchantServiceURL = defaults.url(forKey: "merchantServiceURL")
        applePayMerchantID = defaults.string(forKey: "applePayMerchantID")
    }
}

// MARK: - Helpers
extension GatewayRegion {
    static let all: [GatewayRegion] = [.mtf, .northAmerica, .europe, .asiaPacific, .india]
    
    static func matching(id: String) -> GatewayRegion? {
        var match: GatewayRegion? = nil
        GatewayRegion.all.forEach {
            if $0.id == id {
                match = $0
            }
        }
        return match
    }
}
