//
//  ConfigurationViewController.swift
//  TeebALFkhamh
//
//  Created by intcore-007 on 4/17/20.
//  Copyright © 2020 yogesh. All rights reserved.
//


import UIKit

class ConfigurationViewController: UIViewController {
    
    @IBOutlet weak var merchantIdField: UITextField?
    @IBOutlet weak var regionButton: UIButton?
    @IBOutlet weak var merchantServerUrlField: UITextField?
    @IBOutlet weak var applePayMerchantIdField: UITextField?
    
    @IBOutlet weak var continueButton: UIButton?
    
    var viewModel: ConfigurationViewModel = ConfigurationViewModel() {
        didSet {
            renderViewModel()
        }
    }
    
    override func viewDidLoad() {
        super.viewDidLoad()
        renderViewModel()
    }
    
    override func viewWillAppear(_ animated: Bool) {
        super.viewWillAppear(animated)
        startAvoidingKeyboard()
        viewModel.load()
    }
    
    override func viewDidDisappear(_ animated: Bool) {
        super.viewDidDisappear(animated)
        stopAvoidingKeyboard()
        viewModel.save()
    }
    
    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }
    
    @IBAction func syncMerchantIdField(sender: UITextField) {
        viewModel.merchantId = merchantIdField?.text
    }
    
    @IBAction func syncMerchantServiceUrlField(sender: UITextField) {
        viewModel.merchantServiceURLString = merchantServerUrlField?.text
    }
    
    @IBAction func syncApplePayMerchantIdField(sender: UITextField) {
        viewModel.applePayMerchantID = applePayMerchantIdField?.text
    }
    
    func renderViewModel() {
        merchantIdField?.text = viewModel.merchantId
        regionButton?.setTitle(viewModel.region.name, for: .normal)
        merchantServerUrlField?.text = viewModel.merchantServiceURLString
        applePayMerchantIdField?.text = viewModel.applePayMerchantID
        
        continueButton?.isEnabled = viewModel.isValid
    }
    
    @IBAction func selectRegion(sender: UIView) {
        let alert = UIAlertController(title: "Select a gateway region:", message: nil, preferredStyle: .actionSheet)
        
        alert.modalPresentationStyle = .popover
        alert.popoverPresentationController?.sourceView = sender
        alert.popoverPresentationController?.sourceRect = sender.bounds
        
        viewModel.allRegions.forEach { region in
            let action = UIAlertAction(title: region.name, style: .default, handler: { (_) in
                self.viewModel.region = region
            })
            alert.addAction(action)
        }
        
        present(alert, animated: true, completion: nil)
    }
    
    
    // MARK: - Navigation
    
    // In a storyboard-based application, you will often want to do a little preparation before navigation
    override func prepare(for segue: UIStoryboardSegue, sender: Any?) {
        if let paymentVC = segue.destination as? ProcessPaymentViewController {
            paymentVC.configure(merchantId: viewModel.merchantId!, region: viewModel.region, merchantServiceURL: viewModel.merchantServiceURL!, applePayMerchantIdentifier: viewModel.applePayMerchantID)
        }
    }
    
    
}
