//
/**
ShowMyCartShortCut
@Category Webkul
@author Webkul <support@webkul.com>
FileName: CartShortcut.swift
Copyright (c) 2010-2018 Webkul Software Private Limited (https://webkul.com)
@license   https://store.webkul.com/license.html
*/

import Foundation
import UIKit
import Intents
import CoreSpotlight
import MobileCoreServices



public let kNewArticleActivityType = NetworkManager.sharedInstance.getBundleID()
@available(iOS 12.0, *)
public class CartShortcut{
    public let title: String
    public let content: String
    public let published: Bool
    
    public static func newArticleShortcut(with thumbnail: UIImage?) -> NSUserActivity {
        let activity = NSUserActivity(activityType: kNewArticleActivityType)
       
        activity.persistentIdentifier = NSUserActivityPersistentIdentifier(kNewArticleActivityType)
        activity.isEligibleForSearch = true
        activity.isEligibleForPrediction = true
        let attributes = CSSearchableItemAttributeSet(itemContentType: kUTTypeItem as String)
        // Title
        activity.title = "welocometo".localised+" "+"applicationname".localised
        // Subtitle
        attributes.contentDescription = "You can view cart details"
        // Thumbnail
        attributes.thumbnailData = thumbnail?.jpegData(compressionQuality: 1.0)
        // Suggested Phrase
        activity.suggestedInvocationPhrase = "Show My Cart"
        activity.contentAttributeSet = attributes
        return activity
    }
    

    
    // MARK: - Init
    public init(title: String, content: String, published: Bool) {
        self.title = title
        self.content = content
        self.published = published
    }
    
   
}
