//
/**
TeebALFkhamh
@Category Webkul
@author Webkul <support@webkul.com>
FileName: CartSiriShortcutController.swift
Copyright (c) 2010-2018 Webkul Software Private Limited (https://webkul.com)
@license   https://store.webkul.com/license.html
*/

import UIKit
import IntentsUI



@available(iOS 12.0, *)
class CartSiriShortcutController: UIViewController, INUIAddVoiceShortcutViewControllerDelegate,INUIEditVoiceShortcutViewControllerDelegate {
@IBOutlet var tableView: UITableView!
var voiceShortcut = [INVoiceShortcut]()
    
    
    
    

    override func viewDidLoad() {
        super.viewDidLoad()
        self.getAllShortCut()
    }
    
    
    
    
    @IBAction func siriShortCustClick(_ sender: UIButton) {
        let newArticleActivity = CartShortcut.newArticleShortcut(with: UIImage(named: "ic_cart"))
        let shortcut = INShortcut(userActivity: newArticleActivity)
        let vc = INUIAddVoiceShortcutViewController(shortcut: shortcut)
        vc.delegate = self
        present(vc, animated: true, completion: nil)
    }
    
    func addVoiceShortcutViewController(_ controller: INUIAddVoiceShortcutViewController,
                                        didFinishWith voiceShortcut: INVoiceShortcut?,
                                        error: Error?) {
        self.getAllShortCut()
        dismiss(animated: true, completion: nil)
    }
    
    func addVoiceShortcutViewControllerDidCancel(_ controller: INUIAddVoiceShortcutViewController) {
        dismiss(animated: true, completion: nil)
    }
    
    
    
    
    
    
    // for handling Editing the shortcut
    
    
    func editVoiceShortcutViewController(_ controller: INUIEditVoiceShortcutViewController, didUpdate voiceShortcut: INVoiceShortcut?, error: Error?) {
        self.getAllShortCut()
        controller.dismiss(animated: true, completion: nil)
    }
    
    func editVoiceShortcutViewController(_ controller: INUIEditVoiceShortcutViewController, didDeleteVoiceShortcutWithIdentifier deletedVoiceShortcutIdentifier: UUID){
        self.getAllShortCut()
        controller.dismiss(animated: true, completion: nil)
    }
    
    func editVoiceShortcutViewControllerDidCancel(_ controller: INUIEditVoiceShortcutViewController) {
        controller.dismiss(animated: true, completion: nil)
    }
    
    
    


}




@available(iOS 12.0, *)
extension CartSiriShortcutController:UITableViewDelegate,UITableViewDataSource{
    func getAllShortCut(){
        INVoiceShortcutCenter.shared.getAllVoiceShortcuts { (voiceShortcutsFromCenter, error) in
            if let voiceShortcutsFromCenter = voiceShortcutsFromCenter {
                self.voiceShortcut = voiceShortcutsFromCenter
                print("sfnsjfs",voiceShortcutsFromCenter)
                DispatchQueue.main.async{
                 self.tableView.delegate  = self
                 self.tableView.dataSource = self
                 self.tableView.reloadData()
                }
                
            } else {
                if let error = error as NSError? {
                    print("Failed to fetch voice shortcuts with error: %@")
                }
            }
        }
        
    }
    
    
    
    
    
    func numberOfSections(in tableView: UITableView) -> Int {
        return 1
    }
    
    func tableView(_ tableView: UITableView, titleForHeaderInSection section: Int) -> String? {
        if self.voiceShortcut .count > 0{
           return "availablephrase".localised
        }else{
            return ""
        }
    }
    
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
       return self.voiceShortcut.count
    }
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
      let cell = tableView.dequeueReusableCell(withIdentifier: "settingcell", for: indexPath)
      cell.textLabel?.text = "\"\(self.voiceShortcut[indexPath.row].invocationPhrase)\""
      cell.detailTextLabel?.text = "You can edit or delete this phrase";
      return cell
    }
    
    
    func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath) {
        let vc = INUIEditVoiceShortcutViewController(voiceShortcut:self.voiceShortcut[indexPath.row])
        vc.delegate = self
        self.present(vc, animated: true, completion: nil)
    }
    
}
